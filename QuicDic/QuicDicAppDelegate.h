//
//  QuicDicAppDelegate.h
//  QuicDic
//
//  Created by Michael May on 28/06/2011.
//  Copyright 2011 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kQSMostRecentlyUsedListSize 1

@interface QuicDicAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

#pragma mark - MRU List

-(NSArray*)mostRecentlyUsedList;

-(void)addMostRecentlyUsedEntry:(NSString*)entry;

@end
