//
//  main.m
//  QuicDic
//
//  Created by Michael May on 28/06/2011.
//  Copyright 2011 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QuicDicAppDelegate.h"

int main(int argc, char *argv[])
{
    int retVal = 0;
    @autoreleasepool {
        retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([QuicDicAppDelegate class]));
    }
    return retVal;
}
