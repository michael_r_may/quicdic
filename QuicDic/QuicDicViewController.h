//
//  QuicDicViewController.h
//  QuicDic
//
//  Created by Michael May on 28/06/2011.
//  Copyright 2011 Michael May. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuicDicViewController : UIViewController <UITextFieldDelegate>

@end
