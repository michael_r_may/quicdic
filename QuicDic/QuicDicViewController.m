//
//  QuicDicViewController.m
//  QuicDic
//
//  Created by Michael May on 28/06/2011.
//  Copyright 2011 Michael May. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "QuicDicAppDelegate.h"
#import "QuicDicViewController.h"

@interface QuicDicViewController ()
@property (strong, nonatomic) IBOutlet UILabel *termToDefinePrompt;
@property (strong, nonatomic) IBOutlet UITextField *termToDefine;
@property (strong, nonatomic) IBOutlet UIButton *defineButton;

@property (strong, nonatomic) IBOutlet UIButton *MRUWord1;
@property (strong, nonatomic) IBOutlet UIButton *rateThisAppButton;
@property (strong, nonatomic) IBOutlet UIButton *supportButton;
@end

@implementation QuicDicViewController

#pragma mark - Dismiss The Keyboard

-(IBAction)didRecognizeSwipeDownGesture:(id)sender {
    [[self termToDefine] resignFirstResponder];
}

-(IBAction)didRecognizeSwipeUpGesture:(id)sender {
    [[self termToDefine] becomeFirstResponder];
}

#pragma mark -

-(void)enableDefineButtonForCurrentSearchTerm:(NSString*)searchTerm
{
    BOOL hasSearchTerm = ([searchTerm length] > 0);
    [[self defineButton] setEnabled:hasSearchTerm];
}

-(void)enableDefineButtonForCurrentState
{
    NSString *searchTerm = [[self termToDefine] text];
    [self enableDefineButtonForCurrentSearchTerm:searchTerm];
}

-(void)resetUI {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self termToDefine] setText:@""];
        
        [[self termToDefinePrompt] setText:NSLocalizedString(@"EnterWordToDefinePrompt", @"")];
        
        [[self defineButton] setTitle:NSLocalizedString(@"DefineButtonText", @"") forState:UIControlStateNormal];
        [[self defineButton] setSelected:NO];
        
        [self updateMostRecentlyUsedList];
        
        [self enableDefineButtonForCurrentState];
        
        [self didRotateFromInterfaceOrientation:0x00];
    });
}

-(void)updateMostRecentlyUsedList {
    QuicDicAppDelegate *appDelegate = (QuicDicAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *MRUList = [appDelegate mostRecentlyUsedList];
    NSInteger MRUCount = [MRUList count];
    
    NSString *MRUEntry = @"";
    if(MRUCount > 0) {
        MRUEntry = [MRUList objectAtIndex:0];
    }
    [[self MRUWord1] setTitle:MRUEntry forState:UIControlStateNormal];
}

#pragma mark -

-(void)setDefineButtonAsDefining
{
    [[self defineButton] setTitle:NSLocalizedString(@"DefiningButtonText", @"")
                         forState:UIControlStateNormal];
    [_defineButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [[self defineButton] setEnabled:YES];
}

-(void)setDefineButtonAsDefine
{
    [[self defineButton] setTitle:NSLocalizedString(@"DefineButtonText", @"")
                         forState:UIControlStateNormal];
    
    UIColor *tintColor = [self view].tintColor;
    [[self defineButton] setTitleColor:tintColor forState:UIControlStateNormal];
    [[self defineButton] setEnabled:YES];
}

-(void)defineWord:(NSString*)wordToDefine
{
    if([wordToDefine length] == 0) return;
    
    [[self termToDefine] resignFirstResponder];
    
    [self setDefineButtonAsDefining];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIReferenceLibraryViewController *definitionViewController = [[UIReferenceLibraryViewController alloc] initWithTerm:wordToDefine];
        
        [self presentViewController:definitionViewController
                           animated:YES
                         completion:^(void) {
                             QuicDicAppDelegate *appDelegate = (QuicDicAppDelegate *)[[UIApplication sharedApplication] delegate];
                             [appDelegate addMostRecentlyUsedEntry:wordToDefine];
                         }];
    });
}

#pragma mark - Actions

-(IBAction)defineWordButtonAction:(id)sender {
    NSString *wordToDefine = [[self termToDefine] text];
    [self defineWord:wordToDefine];
}

-(IBAction)defineMRUWordButtonAction:(id)sender {
    UIButton *MRUButton = (UIButton*)sender;
    NSString *MRUButtonTitle = [MRUButton titleForState:UIControlStateNormal];
    
    [[self termToDefine] setText:MRUButtonTitle];
    
    [self defineWord:MRUButtonTitle];
}

-(IBAction)rateTheAppButtonAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=468374861"]];
}

-(IBAction)supportAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://fourapps.wordpress.com/quicdic/support/"]];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    [self enableDefineButtonForCurrentSearchTerm:string];
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self resetUI];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self defineWordButtonAction:nil];
    
    return NO;
}

#pragma mark -

-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - View lifecycle

-(void)addSwipeGestures
{
    UISwipeGestureRecognizer *swipeDownToDismissKeyboard = [[UISwipeGestureRecognizer alloc] init];
    [swipeDownToDismissKeyboard setDirection:UISwipeGestureRecognizerDirectionDown];
    [swipeDownToDismissKeyboard addTarget:self action:@selector(didRecognizeSwipeDownGesture:)];
    [[self view] addGestureRecognizer:swipeDownToDismissKeyboard];
    
    UISwipeGestureRecognizer *swipeUpToCallKeyboard = [[UISwipeGestureRecognizer alloc] init];
    [swipeUpToCallKeyboard setDirection:UISwipeGestureRecognizerDirectionUp];
    [swipeUpToCallKeyboard addTarget:self action:@selector(didRecognizeSwipeUpGesture:)];
    [[self view] addGestureRecognizer:swipeUpToCallKeyboard];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    UIColor *tintColor = [self view].tintColor;
    [self setDefineButtonAsDefine];

    [[self MRUWord1] setTitleColor:tintColor forState:UIControlStateNormal];
    [[self MRUWord1] setContentEdgeInsets:UIEdgeInsetsMake(0.0f, -5.0, 0.0f, 0.0f)];
    
    [[self termToDefine] setPlaceholder:NSLocalizedString(@"WordText", @"")];
    
    [[self rateThisAppButton] setTitle:NSLocalizedString(@"RateThisAppText", @"")
                              forState:UIControlStateNormal];
    [[self rateThisAppButton] setTitleColor:tintColor forState:UIControlStateNormal];
    
    [[self supportButton] setTitle:NSLocalizedString(@"SupportText", @"")
                              forState:UIControlStateNormal];
    [[self supportButton] setTitleColor:tintColor forState:UIControlStateNormal];
    
    [self addSwipeGestures];
    
    [self resetUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self resetUI];
    
    [[self termToDefine] becomeFirstResponder];
}

@end
