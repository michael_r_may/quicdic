//
//  QuicDicAppDelegate.m
//  QuicDic
//
//  Created by Michael May on 28/06/2011.
//  Copyright 2011 Michael May. All rights reserved.
//

#import "QuicDicAppDelegate.h"

@interface QuicDicAppDelegate ()
@end

@implementation QuicDicAppDelegate

#pragma mark - MRU List

#define kQDMostRecentlyUsedListKey @"QDMostRecentlyUsedListKey"

-(NSArray*)mostRecentlyUsedList {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *mostRecentlyUsedList = [userDefaults arrayForKey:kQDMostRecentlyUsedListKey];
    
    if(mostRecentlyUsedList == nil) {
        mostRecentlyUsedList = [NSArray array];
    }
    
    return mostRecentlyUsedList;
}

-(void)addMostRecentlyUsedEntry:(NSString*)entry {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];    
    NSArray *mostRecentlyUsedList = [self mostRecentlyUsedList];
    NSMutableArray *mostRecentlyUsedListMutable = [NSMutableArray arrayWithArray:mostRecentlyUsedList];
    
    [mostRecentlyUsedListMutable insertObject:entry atIndex:0];
    
    while([mostRecentlyUsedListMutable count] > kQSMostRecentlyUsedListSize) {
        [mostRecentlyUsedListMutable removeLastObject];
    }
    
    [userDefaults setObject:mostRecentlyUsedListMutable forKey:kQDMostRecentlyUsedListKey];
    
    [userDefaults synchronize];
}

#pragma mark -

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {        
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)applicationWillTerminate:(UIApplication *)application {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults synchronize];
}

@end
